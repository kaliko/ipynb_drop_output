# Python Notebook output filter

## Setup instructions

Run `python3 ./setup.py install --user` for instance.
It'll install a script named `ipynb_drop_output` in your path.

Alternatively, you can make a manual install:
  * put the filter script in a directory belonging to the system's path.
  * name it `ipynb_drop_output` and make it executable.

Then, setup git filter:
  * register the filter within the git repo:  
    `echo "*.ipynb  filter=clean_ipynb" >> .git/info/attributes # or .gitattributes`
  * set up the filter (adapt the path to the script):  
    `git config --local filter.clean_ipynb.clean /path/to/ipynb_drop_output`  
    `git config --local filter.clean_ipynb.smudge cat`  
    Or you can append the following to your config (local, global or system):  
```
[filter "clean_ipynb"]
     clean = /path/to/ipynb_drop_output
     smudge = cat
```

## Usage instructions

### Within jupyter notebook

The default is to remove output and prompts for a notebook.  
To keep outputs, open the notebook's metadata (Edit > Edit Notebook Metadata). A
panel should open containing some JSON. 
Add an extra line for git, right before the last bracket and don't forget to add an extra comma before:

    "git": { "suppress_outputs": false }

You may need to "touch" the notebooks for git to actually register a change, if
your notebooks are already under version control.

### Command line utility

An helper script `ipynb_git_metadata` can add/remove this metadata for you:

    ipynb_git_metadata --help
    ipynb_git_metadata --add MyNotebook.ipynb


# Credits

Original source: https://gist.github.com/pbugnion/ea2797393033b54674af.  
Which in turn was inspired by http://stackoverflow.com/a/20844506/827862.  

See also this blogpost: http://pascalbugnion.net/blog/ipython-notebooks-and-git.html.

I'm not the original author, I'm just not reinventing the wheel.

#  Alternative projects

 * https://pypi.python.org/pypi/nbstripout
