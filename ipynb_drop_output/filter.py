#!/usr/bin/env python3

import io
import sys
import json

# JSON is UTF-8 encoded by default
# https://stackoverflow.com/a/16549381
INPUT_STREAM = io.TextIOWrapper(sys.stdin.buffer, encoding='utf-8')
OUTPUT_STREAM = io.TextIOWrapper(sys.stdout.buffer, encoding='utf-8')


def strip_output_from_cell(cell):
    if "outputs" in cell:
        cell["outputs"] = []
    if "prompt_number" in cell:
        cell["prompt_number"] = None
    if "execution_count" in cell:
        cell["execution_count"] = None


def main():
    nb = INPUT_STREAM.read()
    json_in = json.loads(nb)
    nb_metadata = json_in["metadata"]
    suppress_output = True
    if "git" in nb_metadata:
        if "suppress_outputs" in nb_metadata["git"]:
            suppress_output = nb_metadata["git"]["suppress_outputs"]
    if not suppress_output:
        OUTPUT_STREAM.write(nb)
        exit()
    ipy_version = int(json_in["nbformat"])-1 # nbformat is 1 more than actual version.
    if ipy_version == 2:
        for sheet in json_in["worksheets"]:
            for cell in sheet["cells"]:
                strip_output_from_cell(cell)
    else:
        for cell in json_in["cells"]:
            strip_output_from_cell(cell)
    json.dump(json_in, OUTPUT_STREAM, sort_keys=True, indent=1, separators=(",",": "), ensure_ascii=False)

if __name__ == '__main__':
    main()
