#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import argparse
import json
import logging
import sys

from argparse import SUPPRESS, FileType

# /Public/ global variable
DEF_LOG_LEVEL = 'info'
# /Private/ global variable
_LOG_FORMAT_S = '{levelname:7}: {message}'
_LOG_FORMAT_D = '{asctime} {levelname:7} {filename}:{lineno}({funcName}): {message}'
_LOG_FORMAT = {
        'debug': _LOG_FORMAT_D,
        'info': _LOG_FORMAT_S,
        'warning': _LOG_FORMAT_S,
        'error': _LOG_FORMAT_S,
            }

ARGS = [# optionnal arguments
        {
            'sw': ['--add'],
            'action': 'store_true',
            'help': 'Add git metadata',
            'default': 'True',
            'dest': 'add_nb_metadata'
            },
        {
            'sw': ['--remove', '--del'],
            'action': 'store_false',
            'help': 'Remove git metadata',
            'dest': 'add_nb_metadata'
            },
        {
            'sw': ['--set-true'],
            'action': 'store_true',
            'help': 'Set git metadata to True (default is False)',
            'default': 'False',
            'dest': 'git_metadata_bool'
            },
        {
            'sw': ['--log-level'],
            'type': str,
            'choices': ['debug', 'info', 'warning', 'error', 'critical'],
            'help': 'Set log level to LEVEL (default: {}).'.format(
                                                          DEF_LOG_LEVEL),
            'default': DEF_LOG_LEVEL,
            'dest': 'log_level',
            },
        # positional arguments
        {
            'sw': ['notebook'],
            'help': 'Jupyter Notebook',
            'type': FileType('r+t'),
            },
       ]


def args(args, conf=None):
    parser = argparse.ArgumentParser(description='',)
    for opt in args:
        opt_names = opt.pop('sw')
        parser.add_argument(*opt_names, **opt)
    return parser

def nb_metadata(args):
    try:
        json_nb = json.load(args.notebook)
    except Exception as err:
        logging.critical('Cannot load notebook "%s": %r', args.notebook.name, err)
        sys.exit(1)
    nb_metadata = json_nb['metadata']
    if 'git' not in nb_metadata:
        logging.info('No git metadata found')
        if not args.add_nb_metadata:
            args.notebook.close()
            logging.debug('Not updating %s', args.notebook.name)
            return
    else: # git metadate exists
        logging.info('Git metadata found: %s', nb_metadata['git'])
        if args.add_nb_metadata:
            if nb_metadata['git'] == {'suppress_outputs': args.git_metadata_bool}:
                args.notebook.close()
                logging.debug('Not updating %s', args.notebook.name)
                return
    if args.add_nb_metadata:
        nb_metadata['git'] = {'suppress_outputs': args.git_metadata_bool}
    if not args.add_nb_metadata:
        nb_metadata.pop('git')
    args.notebook.close()
    with open(args.notebook.name, mode='w') as notebook:
        logging.info('Updating %s', args.notebook.name)
        json.dump(json_nb, notebook, sort_keys=True, indent=1, separators=(",",": "), ensure_ascii=False)


def main():
    parser = args(ARGS)
    pargs = parser.parse_args()
    logging.basicConfig(
            #level=10, #10:debug, 20:info
            level=getattr(logging, DEF_LOG_LEVEL.upper()),
            format=_LOG_FORMAT.get(pargs.log_level),
            style='{',
            datefmt='%Y-%m-%d %H:%M:%S',)
    logging.getLogger().setLevel(getattr(logging, pargs.log_level.upper()))
    nb_metadata(pargs)

# Script starts here
if __name__ == '__main__':
    main()

# VIM MODLINE
# vim: ai ts=4 sw=4 sts=4 expandtab
