#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
"""
from setuptools import setup, find_packages

VERSION = '0.0.3'

CLASSIFIERS = [
    #'Development Status :: 5 - Production/Stable',
    'Development Status :: 4 - Beta',
    'License :: OSI Approved :: GNU General Public License v3 (GPLv3)',
    'Programming Language :: Python :: 3.3',
    'Programming Language :: Python :: 3.4',
    'Programming Language :: Python :: 3.5',
]

setup(
    name="notebook_drop_output",
    version=VERSION,
    classifiers=CLASSIFIERS,
    url='https://gitlab.com/kaliko/ipynb_drop_output',
    license='GPLv3',
    description='Plain filter to remove Cells output',
    author='kaliko jack',
    author_email='kaliko@azylum.org',
    packages=find_packages(),
    zip_safe=False,
    entry_points={
        'console_scripts': [
            'ipynb_git_metadata = ipynb_drop_output.metadata:main',
            'ipynb_drop_output = ipynb_drop_output.filter:main',
            ],
        },
)

# VIM MODLINE
# vim: ai ts=4 sw=4 sts=4 expandtab
